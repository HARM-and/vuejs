import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import loading from 'vuejs-loading-screen'
import Gravatar from 'vue-gravatar';

Vue.component('v-gravatar', Gravatar);

Vue.use(loading)

Vue.prototype.$api = axios.create({
    baseURL: "https://allweb.fun/coop/api/",
    params: {},
    headers: { Authorization: "44526faa9783a58b59cbb8a482631b879c4a55fb" },
});

Vue.prototype.$api.interceptors.request.use(function (config) {
    if (store.state.token) {
        config.params.token = store.state.token;
    }
    return config;
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
